---
title: Lagrangian Mechanics
---

# Mecânica Lagrangiana

## Princípio de D'Alembert e Equações de Lagrange

Um deslocamento virtual de um determinado sistema refere-se a uma alteração na configuração do sistema como resultado de uma qualquer variação arbitrária dessas coordenadas $\delta_{\vec{ri}}$ consistentes com as forças e condicionamentos impostos ao sistema num determinado instante t.

Supondo-se que um sistema está em equilíbrio, i.e, a força total sobre a partícula é nula, 

$$\overrightarrow{F_{i}}=0$$ 

Naturalmente, o trabalho virtual, $\overrightarrow{F_{i}}.\delta_{\vec{ri}}=0$.

A soma destes produtos internos deve anular-se:

$$\sum_{i}{\overrightarrow{F_{i}}.\delta_{\vec{ri}}=0}{(1)}$$ 

$$\overrightarrow{F_{i}}=\overrightarrow{F_{i}^{(a)}} + f_{i}$$

A equação (1) torna-se

$$\sum_{i}\overrightarrow{F_{i}^{(a)}}.\delta_{\vec{ri}} + \sum_{i}f_{i}.\delta_{\vec{ri}} = 0$$

Temos então como **condição** de equilíbrio de um sistema em que o trabalho virtual das forças aplicadas é nulo.

$$\sum_{i}\overrightarrow{F_{i}^{(a)}}.\delta_{\vec{ri}}=0 {(2)}$$
Note-se que os coeficientes de $\delta_{\vec{ri}}$ já não podem ser tomados como nulos, i.e, em geral $\overrightarrow{F_{i}^{(a)}} \neq0$, dado que os $\delta_{\vec{ri}}$ já não são completamente independentes porque estão ligados através dos condicionamentos.

Por forma a igualar os coeficientes a zero, é necessário transformar o princípio numa forma que envolva oa deslocamentos virtuais dos $q_{i}$, que são independentes.

A esperessão (1) satisfaz as nossas necessidades na medida em que não contém os $\overrightarrow{f_{i}}$, mas esta forma lida apenas com estética, é necessário uma condição que envolva o movimento geral do sistema.

A equação do movimento

$\overrightarrow{F_{i}}=\dot{\overrightarrow{p_{i}}}$, pode ser reescrita

$$\overrightarrow{F_{i}}-\dot{\overrightarrow{p_{i}}}=0$$

Reescreve-se (1) como:

$$\sum_{i}{(\overrightarrow{F_{i}}-\dot{\overrightarrow{p_{i}}}).\delta_{\vec{ri}}}=0$$

$$\sum_{i}{(\overrightarrow{F_{i}^{(a)}}-\dot{\overrightarrow{p_{i}}}).\delta_{\vec{ri}}}+\sum_{i}{f_{i}.\delta_{\vec{ri}}}=0$$

Restringindo-nos a sistemas em que o trabalho virtual das forças de condicionamento é nulo, obtem-se:

### Princípio de D'Alembert

* $$\sum_{i}{(\overrightarrow{F_{i}^{(a)}}-\dot{\overrightarrow{p_{i}}}).\delta_{\vec{ri}}}=0$$

Voltando ás transformações $\vec{r_{i}} = \vec{r_{i}}(q_{1}, q_{2},...,q_{n},t)$ assumindo n coordenadas independentes.
Pode-se expressar **$\vec{V_{i}}$** em termos de **$\dot{q_{k}}$**:

$$\vec{V_{i}} \equiv \frac{d\vec{r_{i}}}{dt} = \frac{\partial\vec{r_{i}}}{\partial q_{k}}\dot{q_{k}} + \frac{\partial\vec{{r_{i}}}}{\partial t}$$

Similarmente,

$$\delta_{\vec{ri}}=\sum_{j}{\frac{\partial \vec{r_{i}}}{\partial q_{j}}\delta q_{j}}$$

Obtemos para a forrça $\vec{F_{i}}$, em termos de coordenadas generalizadas 

$$\sum_{i}{\vec{f_{i}}.\delta_{\vec{r_{i}}}}=\sum_{i,j}{\vec{F_{i}}.\frac{\partial \vec{r_{i}}}{\partial q_{j}}\delta q_{j}}=\sum_{j}{Q_{j}\delta q_{j}}$$

$Q_{j}$ são designados por componentes da **força generalizada**.

O outro termo do **P.D'Alembert** pode ser escrito como:

$$\sum_{i}{\dot{\vec{p_{i}}}.\delta_{\vec{r_{i}}}} = \sum_{i}{m_{i}.\ddot{\vec{r_{i}}}.\delta_{\vec{r{i}}}}$$

$$\sum_{i}{m_{i}.\ddot{\vec{r_{i}}}.\delta_{\vec{r{i}}}} = \sum_{i}{[\frac{d}{dt}(m_{i}.\dot{\vec{r_i}}.\delta_{\vec{r_i}})-m_{i}\dot{\vec{r_i}}\frac{d}{dt}\delta_{\vec{r_i}}]} = \frac{d}{dt}(\frac{\partial \vec{r_i}}{\partial q_j}) = \frac{\partial \vec{v_i}}{\partial q_j}$$

$$\sum_{i}{m_i\ddot{\vec{r_i}}\frac{\partial \vec{r_i}}{\partial q_j}} = \sum_i[\frac{d}{dt}(m_i\vec{v_i}.\frac{\partial \vec{v_i}}{\partial \dot{\vec{q_j}}})-m_i\vec{v_i}.\frac{\partial \vec{v_i}}{\partial q_j}], \frac{\partial \vec{r_i}}{\partial q_j} = \frac{\partial \vec{v_i}}{\partial \dot{\vec{q_j}}}$$

$$\sum_i{[\vec{F_i}-\dot{\vec{p_i}}].\delta_{\vec{r_i}}} = 0 \Leftrightarrow \sum_j({\frac{d}{dt}[\frac{\partial }{\partial q_j}(\sum_i{\frac{1}{2}m_iv_i^2})]-\frac{\partial}{\partial q_j}(\sum_i{\frac{1}{2}m_iv_i^2})-Q_j} \Leftrightarrow \sum_j({\frac{d}{dt}(\frac{\partial T}{\partial \dot{q_j}}-\frac{\partial T}{\partial q_j})-Q_j}).\delta q_j = 0$$

Os deslocamentos virtuais são independentes, como tal, a única possibilidade para que a igualdade anterior se verifique é que os coeficientes se anulem individualmente.

$$\frac{d}{dt}(\frac{\partial T}{\partial \dot{q_j}}-\frac{\partial T}{\partial q_j}) = Q_j (1)$$
Quando as forças derivam de uma função potencial escalar, i.e, caso particular das forças conservativas:

$$F_j = -\nabla_jV_j$$

Neste caso as forças generalizadas podem ser escritas:

$$Q_j = - \frac{\partial V}{\partial q_j}$$

A equação (1) pode ser reescrita como:

$$\frac{d}{dt} \frac{\partial T}{\partial \dot{q_j}}-\frac{\partial (T-V)}{\partial q_j} = 0$$

V não depende das velocidades, podemos incluir uma derivada de V em ordem a $\dot{q_j}$ sem alterar os resultados:

$$\frac{\partial V}{\partial \dot{q_J}}=0$$

Obtém-se:

$$\frac{d}{dt} \frac{\partial (T-V)}{\partial \dot{q_j}} - \frac{\partial (T-V)}{\partial q_j} = 0$$

Definindo uma função, o **Lagrangeano** como:

$$L=T-V$$

Obtemos a **equação de Lagrange**:

$$\frac{d}{dt} \frac{\partial L}{\partial \dot{q_j}} - \frac{\partial L}{\partial q_j} = 0$$

### Potenciais que dependem da velocidade

$$F_x = -k_xv_x$$

Introduz-se uma função _F_, designada por **função de dissipação de Raleigh**:

_$F$_ = $\frac{1}{2}\sum_i{[k_xv_{ix}^2+k_yv_{iy}^2+k_zv_{iz}^2]}$

$$F_{(x,y,z)} = -\frac{\partial F}{\partial v_{(x,y,z)}}$$

$$Q_j = -\frac{\partial F}{\partial \dot{q_j}}$$

Então, obtemos as **equações de Lagrange com dissipação**:

$$\frac{d}{dt}(\frac{\partial L}{\partial \dot{q_J}})-\frac{\partial L}{\partial q_j} + \frac{\partial F}{\partial \dot{q_j}} = 0$$
