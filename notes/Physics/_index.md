
---
title: Physics
chapter: true
weight: 1
---

# Introdução {epub:type=preface}

Física é a ciência que estuda a natureza e seus fenômenos em seus aspectos mais gerais. Analisa suas relações e propriedades, além de descrever e explicar a maior parte de suas consequências. Busca a compreensão científica dos comportamentos naturais e gerais do mundo em nosso torno, desde as partículas elementares até o universo como um todo.