
---
title: Mechanics
weight: 1
---

# Mecânica

A mecânica é o ramo da física que compreende o estudo e análise do movimento e repouso dos corpos, a sua evolução no tempo, os seus deslocamentos, sob a ação de forças, e os seus efeitos subsequentes sobre o seu ambiente.